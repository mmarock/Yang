

using System.Collections.Generic;
using System.Diagnostics;

namespace straggling {

/** 
 * @brief some beam parameter
 */
struct Beam {
	// equals Z
	public uint atom_number;
	// equals A
	public uint atom_mass;
	// and the beam energy
	public List<double> energies;

	public Beam(uint _atom_number, uint _atom_mass,
			List<double> _energies) {
		atom_number = _atom_number;
		atom_mass = _atom_mass;
		energies = _energies;

		Debug.WriteLine(string.Format(
			"Beam: created with Z:{0} A:{0}",
			atom_number, atom_mass));
	}
} // struct Beam



} // namespace beam

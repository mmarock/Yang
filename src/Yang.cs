

using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace straggling {

/**
 * @brief Yang straggling, copied from yang3.py
 */
class Yang {

	////////////////////////////////////////
	/// 	private member
	////////////////////////////////////////
	private Beam beam_;
	private Foil foil_;

	////////////////////////////////////////
	/// 	public member
	////////////////////////////////////////

	public Yang(Beam beam, Foil foil)
	{
		beam_ = beam;
		foil_ = foil;
	}

	public double straggle()
	{
		List<Foil.Partition> partitions=foil_.partition();
		double sigmas = 0;
		foreach (double energy in beam_.energies) {
			// calculate the sigma to the given energy
			double sigma = 0;
			foreach (Foil.Partition p in partitions) {
				sigma = sqrt(squared(sigma) + 
					eq_five(energy, beam_, p) * bohr(beam_, p));
			}
			// add the sigma to overlap
			sigmas = sqrt(squared(sigmas) + squared(sigma));
		}
		return sigmas;
	}

	////////////////////////////////////////
	/// 	private convenience wrapper
	////////////////////////////////////////
	
	private static double squared(double val) {
		return Math.Pow(val, 2);
	}

	private static double sqrt(double val) {
		return Math.Sqrt(val);
	}

	////////////////////////////////////////
	/// 	private static member
	////////////////////////////////////////

	private static double bohr(Beam beam, Foil.Partition partition)
	{
		return 1.57 * 0.0001 * squared(beam.atom_number) *
		       partition.atom_number * partition.density /
		       partition.atom_mass;
	}

	private static double gamma(double energy, uint z_beam)
	{

		return sqrt(
			1 - Math.Exp(-0.92 / Math.Pow(z_beam, 2.0 / 3.0) *
			sqrt(energy / 0.025)));
	}

	private static double eq_eight(double energy, uint z_beam,
				       uint z_target)
	{
		return energy / Math.Pow(z_beam, 1.5) /
		       sqrt(z_target);
	}

	private static double eq_seven_a(double energy, uint z_beam,
					 uint z_target)
	{
		double C1 = Coefficients.C(Coefficients.State.Solid, 1);
		double C2 = Coefficients.C(Coefficients.State.Solid, 2);
		double gamma = eq_seven_b(energy, z_beam, z_target);
		double epsilon = eq_eight(energy, z_beam, z_target);
		return (Math.Pow(z_beam, 4.0 / 3.0) /
			Math.Pow(z_target, 1.0 / 3.0)) *
		       C1 * gamma /
		       (squared(epsilon - C2) + squared(gamma));
	}

	private static double eq_seven_b(double energy, uint z_beam,
					 uint z_target)
	{
		double C3 = Coefficients.C(Coefficients.State.Solid, 3);
		double C4 = Coefficients.C(Coefficients.State.Solid, 4);
		double res_eq_eight = eq_eight( energy, z_beam, z_target);
		return C3 * (1.0 - Math.Exp(-C4 * res_eq_eight));
	}

	private static double eq_four(double energy, uint atom_number)
	{
		double A1 = Coefficients.A(atom_number, 1);
		double A2 = Coefficients.A(atom_number, 2);
		double A3 = Coefficients.A(atom_number, 3);
		double A4 = Coefficients.A(atom_number, 4);
		return 1.0 / (1.0 + A1 * Math.Pow(energy, A2) +
			      A3 * Math.Pow(energy, A4));
	}

	private static double eq_five(double energy, Beam beam,
				      Foil.Partition partition)
	{
		double energy_per_mass = energy / (double)beam.atom_mass;
		double res_gamma = gamma(energy_per_mass, beam.atom_number);
		double res_eq_four = eq_four(energy_per_mass,
				partition.atom_number);
		double res_eq_seven_a = eq_seven_a(energy_per_mass,
				beam.atom_number, partition.atom_number);
		return squared(res_gamma) * res_eq_four  + res_eq_seven_a;
	}

} // class Yang

} // namespace straggling

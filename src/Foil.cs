

using System.Collections.Generic;
using System.Diagnostics;

namespace straggling {

class Foil {

////////////////////////////////////////
/// 	public structs
////////////////////////////////////////

	public struct Compound {
		public uint atom_number;
		public uint atom_mass;
		public double share;
	} // struct Compound

	public struct Partition {
		public uint atom_number;
		public uint atom_mass;
		public double density;
	} // struct Partition

////////////////////////////////////////
/// 	private member
////////////////////////////////////////

	private double density_;
	private double thickness_;
	private List<Compound> compounds_;

////////////////////////////////////////
/// 	public member
////////////////////////////////////////

	public Foil(double density, double thickness, List<Compound> compounds) {
		density_ = density;
		thickness_ = thickness;
		compounds_ = compounds;

		Debug.WriteLine(string.Format(
			"Foil: density:{0} thickness:{1} len(compounds):{2}",
			density, thickness, compounds_.Count));
	}

	public List<Partition> partition() {

		// first total share
		double total = 0;
		foreach (Compound c in compounds_) {
			total += (double)c.atom_mass*c.share;
		}

		// calc density of each component
		List<Partition> partitions = new List<Partition>();
		foreach (Compound c in compounds_) {
			double density = density_ * Foil.conversion_factor_ *
				c.atom_mass * c.share * thickness_ / total;

			partitions.Add( new Partition {
						atom_number = c.atom_number,
						atom_mass = c.atom_mass,
						density = density
						} );
		}

		// debug only
		foreach(Partition p in partitions) {
			Debug.WriteLine(string.Format(
				"Partition: Z:{0:000} A:{1:000} density:{2:e0}",
				p.atom_number, p.atom_mass, p.density));
		}
		return partitions;
	}


////////////////////////////////////////
///	private static member
////////////////////////////////////////

	// conversion factor to the molar mass / 100
	private const double conversion_factor_ = 6.022*1.6605/100000.0;

} // class Foil


} // namespace straggling

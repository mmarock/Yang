

using straggling;
using System.Xml;
using System.Collections.Generic;
using System.Diagnostics;

namespace  straggling {

public static class YangTest {



	static void Main(string[] args) {



		TextWriterTraceListener myWriter = new TextWriterTraceListener(System.Console.Out);
		Debug.Listeners.Add(myWriter);


		uint Z = 26;
		uint A = 54;
		double E = 94;
		double Eloss = 17.1;
		uint steps = 1000;

		double density = 2.7;
		double thickness = 1940;


		List<Foil.Compound> compounds = new List<Foil.Compound>();
		compounds.Add(new Foil.Compound{
				atom_number = 14,
				atom_mass = 28,
				share = 3,
		});
		compounds.Add(new Foil.Compound{
				atom_number = 7,
				atom_mass = 14,
				share = 3.75,
		});


		List<double> energies = new List<double>();
		for (uint i = 0; i < steps; ++i) {
			energies.Add(E - (double)i/(double)steps*Eloss);
		}

		thickness = thickness/steps;

		Beam beam = new Beam(Z,A,energies);
		Foil foil = new Foil(density, thickness, compounds);

		Yang yang = new Yang(beam, foil);
		double res = yang.straggle();
		System.Console.Out.WriteLine("result: " + res);


		/*
		XmlDocument doc = parse_config("input.xml");

		List<double> energies;

		beam_node = doc.GetElementById("beam");
		foil_node = doc.GetElementById("foil");

		int atom_number_beam = beam_node.GetElementById("atom_number").InnerXml;
		double atom_mass_beam = beam_node.GetElementById("atom_mass").InnerXml;
		double beam_energy = beam_node.GetElementById("energy").InnerXml;
		double energy_loss = beam_node.GetElementById("energy_loss").InnerXml;
		int steps = doc.GetElementById("steps").InnerXml;
		


		for (int i = 0; i < steps; ++i) {
			energies.Add(
					);
		}

		*/


	
	
	}

	/*
	static XmlDocument parse_config(string path) {
		// open stream, read doc, close
	}
	*/

} // class YangTest



} // namespace straggling
